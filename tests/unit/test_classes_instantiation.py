
from sle.service.base import SleUser
from sle.datatypes.bind import SleBindInvocation, SleBindReturn

def test_SleUser_instantiation():
    sleuser = SleUser(
        "sagr=xyz.spack=abcdef.rsl-fg=gfjdy.raf=onlc2",  # service_instance_identifier
        "0.0.0.0",  # responder_host
        "20001",  # responder_port
        "none",  # auth_level
        "username_local",  # local_identifier
        "username_peer",  # peer_identifier
        provider_to_user_pdu=SleBindInvocation,  # necessary, SLE Protocol Data Unit 
        user_to_provider_pdu=SleBindReturn  # necessary, SLE Protocol Data Unit 
    )

    assert sleuser
    assert sleuser.state == "unbound"
