# API Documentation

All user services expect the following mandatory parameters:

- `service_instance_identifier`: A string that must adhere to the CCSDS defined service instance format
- `responder_host`: The host address of the SLE provider
- `responder_port`: The port of the SLE provider
- `auth_level`: Whether authentication is used: `"bind"`, `"all"`, or `"none"`.
- `local_identifier`: The name of the SLE user as string.
- `peer_identifier`: The name of the SLE provider as string.
- `local_password`: A string with hex nibbles.
- `peer_password`: A string with hex nibbles.

Note that identifiers and passwords must be configured at the SLE user and
provider in order to work.

The following parameters are optional:

- `version_number`: The version of the CCSDS standard to use. Default is 5.
- `heartbeat`: Default is 25.
- `deadfactor`: Default is 5
- `buffer_size`: Default is 256000

## RafUser

The RAF service returns all frames received from the SLE Provider. Thus it
is useful for testing of there are frames received by the ground station.

First create an instance of the RAF service:

```python
import sle

raf_service = sle.RafServiceUser(
    service_instance_identifier=SI_IDENTIFIER,
    responder_host=RESPONDER_HOST,
    responder_port=RESPONDER_PORT,
    auth_level="bind",  # or "all" or None
    local_identifier=LOCAL_IDENTIFIER,
    peer_identifier=PEER_IDENTIFIER,
    local_password=LOCAL_PASSWORD,
    peer_password=PEER_PASSWORD)

```

The first and last thing to do for a service session is to bind and unbind:

```python
raf_service.bind()
#...
raf_service.unbind()

```

When a session is bound, one can start it in order to receive frames. Afterwards
stop it:

```python
raf_service.start()
time.sleep(10)  # wait for 10 seconds to show incoming frames
raf_service.stop()

```

## RcfUser

The RCF service is similar to RAF, except that it only returns the frames of
a certain virtual channel. An example:

```python
import time
import sle

rcf_service = sle.RcfServiceUser(
    service_instance_identifier=SI_IDENTIFIER,
    responder_host=RESPONDER_HOST,
    responder_port=RESPONDER_PORT,
    auth_level="bind",  # or "all" or None
    local_identifier=LOCAL_IDENTIFIER,
    peer_identifier=PEER_IDENTIFIER,
    local_password=LOCAL_PASSWORD,
    peer_password=PEER_PASSWORD)

def print_data(data):
    print(data.prettyPrint())

rcf_service.frame_indication = print_data

rcf_service.bind()
time.sleep(1)

input("Enter to start")
rcf_service.start(gvcid=(844, 0, 0))
time.sleep(1)

input("Enter to stop")
rcf_service.stop()
time.sleep(1)

input("Enter to unbind")
rcf_service.unbind()
time.sleep(1)

```

## CltuUser

For uplink of commands the CLTU service is to be used. Commands are embedded
in so-called Command Link Transmission Units (CLTUs), which is out of the
scope of this package. It is expected that the CLTUs are prepared by the
user and then passed on as byte arrays.

```python
import time
import sle

cltu_service = sle.CltuServiceUser(...)  # as above

cltu_service.bind()
time.sleep(1)

input("Enter to start")
cltu_service.start()
time.sleep(1)

cltu = b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a"  # dummy example
cltu_service.transfer_data(cltu)

input("Enter to stop")
cltu_service.stop()
time.sleep(1)

input("Enter to unbind")
cltu_service.unbind()
time.sleep(1)

```
